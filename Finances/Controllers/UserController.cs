﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finances.Models;
using Finances.DAO;
using WebMatrix.WebData;
using System.Web.Security;

namespace Finances.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private UserDAO userDAO;

        public UserController(UserDAO userDAO)
        {
            this.userDAO = userDAO;
        }
        // GET: User
        public ActionResult Index()
        {
            IList<User> users = userDAO.List();
            return View(users);
        }
        public ActionResult Form()
        {
            return View();
        }
        public ActionResult Add(ViewUser user)
        {
            if (ModelState.IsValid) {
                try
                {
                    WebSecurity.CreateUserAndAccount(user.name, user.passwd, new { email = user.email });
            
                    return RedirectToAction("Index");
                }
                catch(MembershipCreateUserException e)
                {
                    ModelState.AddModelError("user.Invalid", e.Message);
                    return View("Form", user);
                }
             
            }
            else {
                return View("Form", user);
            }
        }
    }
}