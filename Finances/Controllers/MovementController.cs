﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finances.DAO;
using Finances.Models;
namespace Finances.Controllers
{
    [Authorize]
    public class MovementController : Controller
    {
        private MovementDAO movementDAO;
        private UserDAO userDAO;

        public MovementController(MovementDAO movementDAO, UserDAO userDAO)
        {
            this.movementDAO = movementDAO;
            this.userDAO = userDAO;
        }
        // GET: Movement
        public ActionResult Form()
        {
            ViewBag.Users = userDAO.List();
            return View();
        }

        public ActionResult Add(Movement movement)
        {
            if (ModelState.IsValid)
            {
                movementDAO.Add(movement);
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Users = userDAO.List();
                return View("Form", movement);
            }
        }

        public ActionResult Index()
        {
            IList < Movement > movements = movementDAO.List();
            return View(movements);
        }

        public ActionResult sMovementsByUser(ViewMovement model)
        {
            model.users = userDAO.List();
            model.movements = movementDAO.sByUser(model.userId);
            return View(model);
        }

        public ActionResult sAllMovements(ViewSMovement model)
        {
            model.users = userDAO.List();
            model.movements = movementDAO.sAll(model.minDate, model.maxDate,
                model.minValue, model.maxValue, model.type, model.userId);

            return View(model);
        }
    }
}