﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace Finances.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Auth(String login, String passwd)
        {
           if(WebSecurity.Login(login, passwd))
            {
                return RedirectToAction("Index","Home");
            }
            else
            {
                ModelState.AddModelError("login.Invalid", "Login or password dont match");
                return View("Index");
            }
        }

        public ActionResult Logout()
        {
            WebSecurity.Logout();
            return RedirectToAction("Index");
        }
    }
}