﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Finances.Models;

namespace Finances.DAO
{
    public class FinancesContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Movement> Movements { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movement>().HasRequired(m => m.user);
        }
    }
}