﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Finances.Models;

namespace Finances.DAO
{
    public class UserDAO
    {
        private FinancesContext context;

        public UserDAO(FinancesContext context)
        {
            this.context = context;
        }

        public void Add(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
        }

        public IList<User> List()
        {
            return context.Users.ToList();
        }
    }
}