﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Finances.Models;

namespace Finances.DAO
{
    public class MovementDAO
    {
        private FinancesContext context;

        public MovementDAO(FinancesContext context)
        {
            this.context = context;
        }

        public void Add(Movement movement)
        {
            context.Movements.Add(movement);
            context.SaveChanges();
        }

        public IList<Movement> List()
        {
            return context.Movements.ToList();
        }

        public IList<Movement> sByUser(int? userId)
        {
            return context.Movements.Where(m => m.userId == userId).ToList();
        }

        public IList<Movement> sAll(DateTime? minDate, DateTime? maxDate, decimal? minValue, decimal? maxValue, Models.Type? type, int? userId)
        {
           IQueryable<Movement> s = context.Movements;
            if (minValue.HasValue)
            {
                s = s.Where(m => m.value >= minValue);
            }
            if (maxValue.HasValue)
            {
                s = s.Where(m => m.value <= maxValue);
            }
            if (minDate.HasValue)
            {
                s = s.Where(m => m.date >= minDate);
            }
            if (maxDate.HasValue)
            {
                s = s.Where(m => m.date <= maxDate);
            }
            if (type.HasValue)
            {
                s = s.Where(m => m.type == type);
            }
            if (userId.HasValue)
            {
                s = s.Where(m => m.userId == userId);
            }

            return s.ToList();



        }
    }


}