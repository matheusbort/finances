namespace Finances.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialTables1 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Movements", name: "user_id1", newName: "userId");
            RenameIndex(table: "dbo.Movements", name: "IX_user_id1", newName: "IX_userId");
            DropColumn("dbo.Movements", "user_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Movements", "user_id", c => c.Int(nullable: false));
            RenameIndex(table: "dbo.Movements", name: "IX_userId", newName: "IX_user_id1");
            RenameColumn(table: "dbo.Movements", name: "userId", newName: "user_id1");
        }
    }
}
