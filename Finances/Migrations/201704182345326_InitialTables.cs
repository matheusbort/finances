namespace Finances.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Movements",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        date = c.DateTime(nullable: false),
                        type = c.Int(nullable: false),
                        user_id = c.Int(nullable: false),
                        user_id1 = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Users", t => t.user_id1, cascadeDelete: true)
                .Index(t => t.user_id1);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Movements", "user_id1", "dbo.Users");
            DropIndex("dbo.Movements", new[] { "user_id1" });
            DropTable("dbo.Users");
            DropTable("dbo.Movements");
        }
    }
}
