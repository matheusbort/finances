﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finances.Models
{
    public class ViewMovement
    {
        public int? userId { get; set; }

        public IList<Movement> movements { get; set; }

        public IList<User> users { get; set; }
    }
}