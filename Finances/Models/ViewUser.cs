﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Finances.Models
{
    public class ViewUser
    {
        [Required]
        public string name { get; set; }
        [Required, EmailAddress]
        public string email { get; set; }
        [Required]
        public string passwd { get; set; }
        [Compare("passwd")]
        public string confirmpasswd { get; set; }

    }
}