﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finances.Models
{
    public class ViewSMovement
    {
        public decimal? minValue { get; set; }
        public decimal? maxValue { get; set; }
        public DateTime? maxDate { get; set; }
        public DateTime? minDate { get; set; }
        public Type? type { get; set; }
        public int? userId { get; set; }

        public IList<Movement> movements { get; set; }
        public IList<User> users { get; set; }
    }
}