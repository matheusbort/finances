﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Finances.Models
{
    public class Movement
    {
        public int id { get; set; }
        public decimal value { get; set; }
        public DateTime date { get; set; }
        public Type type { get; set; }
        public int userId  { get; set; }
        public virtual User user { get; set; }
    }
}